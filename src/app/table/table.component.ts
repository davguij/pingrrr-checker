import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {Observable} from 'rxjs/Rx';

@Component({
	selector: 'app-table',
	templateUrl: 'table.component.html',
	styleUrls: ['table.component.css']
})
export class TableComponent implements OnInit {

	subscription;
	results: Object[];
	average: Number;
	loading: Boolean;
	grabberUri = 'https://d4pz0r42hk.execute-api.ap-southeast-1.amazonaws.com/dev/grab';

	constructor(private http: Http) { }

	ngOnInit() {
		this.loading = true;
		this.subscription = this.getResults()
			.subscribe(retrievedResults => {
				this.results = retrievedResults.items;
				this.average = retrievedResults.average;
				this.loading = false;
			});
	}

	ngOnDestroy() {
		this.subscription.unsubscribe();
	}

	getResults() {
		return this.http.get(this.grabberUri)
			.map(res => res.json())
			.catch((err:any) => Observable.throw(err));
	}

}
