import { Component, ViewContainerRef, ViewEncapsulation } from '@angular/core';

import { Overlay } from 'angular2-modal';
import { Modal } from 'angular2-modal/plugins/bootstrap';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {

	constructor(overlay: Overlay, vcRef: ViewContainerRef, public modal: Modal) {
		    overlay.defaultViewContainer = vcRef;
	 }

	showModal() {
		this.modal.alert()
			.size('lg')
			.showClose(true)
			.isBlocking(false)
			.title("What's this?")
			.body(`<p><strong>Pingrrr</strong> is basically an experiment and is probably not that useful.</p>
				<p>It basically requests a certain DotRez availability page each 30 minutes and saves how long the response actually takes.</p>
				<p>Then it shows all the saved data, including how slow or fast each request was when compared to the accumulated average response time.</p>
				<hr />
				<p class="lead">Made with love by David Guijarro for the kind people at Air Asia, using AWS Lambda, DynamoDB and Angular 2.</p>
			`)
			.open();
	}
// modal.prompt()

}
	// 		.size('sm')
	// 		.isBlocking(false)
	// 		.showClose(true)
	// 		.keyboard(27)
	// 		.title('What's this?')
	// 		.body('"Pingrrr" is basically an experiment and is probably not that useful.
	// 	What is does is requesting a DotRez availability page each 30 minutes and save how long the request actually takes. Then it shows all the saved data with some hints to keep track of the booking engine performance.
	// 	Made with love by David Guijarro for the kind people at Air Asia, using AWS Lambda, DynamoDB and Angular 2.')
	// 		.okBtnClass('btn')
    // .open();
